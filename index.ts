import { Telegraf , Markup, Extra, Context } from 'telegraf';
import yaml = require('js-yaml');
import fs = require('fs');
import { createHash } from 'crypto';
import commandLineArgs = require('command-line-args');
import { Client } from 'pg';

const optionDefinitions = [
  { name: 'conf-dir', alias: 'c', type: String },
  { name: 'work-dir', alias: 'w', type: String }
]


const clops = commandLineArgs(optionDefinitions)

const db_user : string = yaml.load(fs.readFileSync(`${clops['conf-dir']}/postgresql.yaml`,'utf8'))['user'];
const db_pass : string = yaml.load(fs.readFileSync(`${clops['conf-dir']}/postgresql.yaml`,'utf8'))['password'];
const token : string = yaml.load(fs.readFileSync(`${clops['conf-dir']}/bot.yaml`,'utf8'))['token'];
const my_id : number = yaml.load(fs.readFileSync(`${clops['conf-dir']}/I.yaml`,'utf8'))['id'];

const connstr = `postgresql://${db_user}:${db_pass}@localhost/`;


const lists_file = `${clops['work-dir']}/lists.json`

const commands_help = 
  "✨/menu  " +
  "\ud83d\udcd5✨/todo " +
  "\ud83d\udcd5✏/rem " +
  "\ud83d\udcda/lists " +
  "\ud83d\udcd7/list " + 
  "\ud83d\udcd7✏/add " +
  "\ud83d\udcd7➕╱newlist ... " + 
  "\ud83d\udcd7➖╱delist " +
  "\ud83e\uddf9/clear "

enum StateKind { NormalOp , TodoDeleteOp , ItemDeleteOp , ListDeleteOp , EnterNewRemOp , EnterNewItemOp }
interface NormalOperation { kind : StateKind.NormalOp } 
interface TodoDeleteConfirmation { kind : StateKind.TodoDeleteOp ; checksum : string ; }
interface ItemDeleteConfirmation { kind : StateKind.ItemDeleteOp ; checksum : string ; }
interface ListDeleteConfirmation { kind : StateKind.ListDeleteOp ; checksum : string ; }
interface EnterNewRem { kind : StateKind.EnterNewRemOp }
interface EnterNewItem { kind : StateKind.EnterNewItemOp }
type State = 
  NormalOperation | 
  TodoDeleteConfirmation | 
  ItemDeleteConfirmation | 
  ListDeleteConfirmation | 
  EnterNewRem | 
  EnterNewItem

let state : State = { kind : StateKind.NormalOp } as NormalOperation

enum DefaultListKind { DLExists , DLDoesNotExist }
interface DefaultList { kind : DefaultListKind.DLExists, id : string }
interface NoDefaultList { kind: DefaultListKind.DLDoesNotExist }
type CurrentDefaultList = DefaultList | NoDefaultList

let currentList : CurrentDefaultList = { kind : DefaultListKind.DLDoesNotExist } as NoDefaultList

//type LM = { [key:string] : string[] }

//console.log("Starting Bot")

type LstMp = Map<string, string[]>

function sha(x:string) : string {
  return createHash('sha1').update(x).digest('hex');
}

async function todolist() : Promise<string[]> {
  // if (! fs.existsSync(todolist_file)) { fs.writeFileSync(todolist_file, JSON.stringify([])); }
  // return JSON.parse(fs.readFileSync(todolist_file,'utf8'));
  const client = new Client({connectionString: connstr});
  await client.connect();
  return client.query('SELECT todo from todolist').then(
    function(res) { 
      client.end();
      return res.rows.map(function(row) {return row.todo}) ;
    }
  )
  
}

async function save_todolist(newlist: string[]) {
  //if (! fs.existsSync(todolist_file)) { fs.writeFileSync(todolist_file, JSON.stringify([])); }
  //fs.writeFileSync(todolist_file,JSON.stringify(j));
  const client = new Client({connectionString: connstr});
  await client.connect();
  await client.query('BEGIN');
  await client.query('DELETE from todolist').then(
    function() {
      return Promise.all(
        newlist.map (
          function(itm) {
            //console.log(`inserting ----->${x}<---`);
            client.query(
              'INSERT INTO todolist(todo) VALUES ($1)',
              [itm]
            )
          }
        )
      ).then(
      function() {
        console.log(`inserted ${newlist}`);
        return client.query('COMMIT');
      }, 
      function(reason) {
        console.log(`PROBLEM with SQL INSERT: ${reason}`);
        return client.query('ROLLBACK');
      }
      )
    },
    function(reason) {
      console.log(`PROBLEM with SQL DELETE: ${reason}`);
      return client.query('ROLLBACK');
    }
  );
  await client.end();
}

function all_lists() : LstMp {
  if (! fs.existsSync(lists_file)) { fs.writeFileSync(lists_file, JSON.stringify({})); }
  const j = JSON.parse(fs.readFileSync(lists_file,'utf8')) 
  const m : LstMp = new Map()
  Object.keys(j).forEach((k) => { m.set(k,j[k]) })
  return m
}

function save_lists(m: LstMp) {
  if (! fs.existsSync(lists_file)) { fs.writeFileSync(lists_file, JSON.stringify({})); }
  const o : {[k : string] : string[]} = {}
  for (const k of m.keys()) { o[k] = m.get(k) }
  fs.writeFileSync(lists_file,JSON.stringify(o));
}

async function rem(c:Context) {
  const tdl = await todolist();
  c.reply(
    "TODO:",
    Extra.markdown().markup(
      Markup.inlineKeyboard(tdl.map((itm) => [Markup.callbackButton(itm, `TODODEL:${sha(itm)}`)]))
    )
  )
}

function lists(ctx:Context) {
  const lsts : LstMp = all_lists()
  ctx.reply(
    "Lists:",
    Extra.markdown().markup(
      Markup.inlineKeyboard(Array.from(lsts.keys()).map((k) => [Markup.callbackButton(k, `CURLIST:${sha(k)}`)]))
    )
  )
}

function show_list(ctx:Context) {
  switch (currentList.kind) {
    case (DefaultListKind.DLDoesNotExist) : {
      lists(ctx)
      break
    }
    case (DefaultListKind.DLExists) : {
      const lsts : LstMp = all_lists()
      const lst : string[] = lsts.get((currentList as DefaultList).id)
      ctx.reply(
        `List ${(currentList as DefaultList).id} :`,
        Extra.markdown().markup(
          Markup.inlineKeyboard(lst.map((itm) => [Markup.callbackButton(itm, `ITEMDEL:${sha(itm)}`)]))
        )
      )
      break
    }
  }
}

function menu(ctx:Context) {
  ctx.reply(
    commands_help,
    Extra.markdown().markup(Markup.keyboard(
      [
        ["/todo", "/rem"], 
        ["/list", "/add", "/lists"], 
        ["/menu"]
      ]).oneTime().resize())
  )
}

const bot = new Telegraf(token)

bot.use(async (ctx, next) => {
  if (ctx.from['id'] === my_id) { 
    return next() ; 
  } else { 
    ctx.reply(`How are you ${JSON.stringify(ctx.from)}`) ; 
  }
})

bot.use(async (ctx, next) => {
  if (ctx.message !== undefined && ctx.message.text == '/clear') {
    state = { kind : StateKind.NormalOp } as NormalOperation
    ctx.reply(
      "\ud83e\uddf9 \ud83e\uddf9 \ud83e\uddf9" + "\n" + commands_help,
      Extra.markdown().markup(Markup.removeKeyboard())
    )
  } else { return next(); }
})


bot.use(async (ctx, next) => {
  switch(state.kind) {
    case StateKind.NormalOp : {
      //ctx.reply("-----------------")
      return next();
    }
    case StateKind.TodoDeleteOp : {
      if ( ctx.message == undefined ) {
        state = { kind : StateKind.NormalOp } as NormalOperation
        return next(); 
      } else if ( ctx.message.text === 'Yes') {
        const remains: string[] = [];
        const tdl = await todolist();
        tdl.forEach(v => {
          if  (sha(v) === (state as TodoDeleteConfirmation).checksum) { ctx.reply(`deleted: ${v}`); } else { remains.push(v); }
        })
        await save_todolist(remains);
        state = { kind : StateKind.NormalOp } as NormalOperation
        rem(ctx)
      } else if ( ctx.message.text === 'No' ) {
        state = { kind : StateKind.NormalOp } as NormalOperation 
        rem(ctx)
      }
      break
    }
    case StateKind.ItemDeleteOp : {
      if ( ctx.message == undefined ) {
        state = { kind : StateKind.NormalOp } as NormalOperation
        return next(); 
      } else if ( ctx.message.text === 'Yes') {
        switch (currentList.kind) {
          case (DefaultListKind.DLExists) : {
            const lsts : LstMp = all_lists();
            const curId = (currentList as DefaultList).id;
            const lst = lsts.get(curId) as string[]
            const remains: string[] = [];
            lst.forEach(v => {
              if  (sha(v) === (state as ItemDeleteConfirmation).checksum) { ctx.reply(`deleted: ${v}`); } else { remains.push(v); }
            })
            lsts.set(curId, remains)
            save_lists(lsts)
            state = { kind : StateKind.NormalOp } as NormalOperation
            show_list(ctx)
            break
          }
          case (DefaultListKind.DLDoesNotExist) : {
            state = { kind : StateKind.NormalOp } as NormalOperation
            ctx.reply("ERROR: no default list...")
            break
          }
        }
      } else if ( ctx.message.text === 'No' ) {
        state = { kind : StateKind.NormalOp } as NormalOperation 
        show_list(ctx)
      }
      break
    }
    case StateKind.ListDeleteOp : {
      if ( ctx.message == undefined ) {
        state = { kind : StateKind.NormalOp } as NormalOperation
        return next(); 
      } else if ( ctx.message.text === 'Yes') {
        switch (currentList.kind) {
          case (DefaultListKind.DLExists) : {
            const lsts: LstMp = all_lists();
            const remains: LstMp = new Map();
            for (const k of lsts.keys()) {
              if  (sha(k) === (state as ListDeleteConfirmation).checksum) { 
                ctx.reply(`deleted list: ${k}`); 
              } else { 
                remains.set(k,lsts.get(k)); 
              }
            }
            save_lists(remains)
            currentList = { kind : DefaultListKind.DLDoesNotExist } as NoDefaultList
            state = { kind : StateKind.NormalOp } as NormalOperation
            lists(ctx)
            break
          }
          case (DefaultListKind.DLDoesNotExist) : {
            state = { kind : StateKind.NormalOp } as NormalOperation
            ctx.reply("ERROR: no default list...")
            break
          }
        }
      } else if ( ctx.message.text === 'No' ) {
        state = { kind : StateKind.NormalOp } as NormalOperation 
        show_list(ctx)
      }
      break
    }
    case StateKind.EnterNewRemOp : {
      if ( ctx.message == undefined ) {
        state = { kind : StateKind.NormalOp } as NormalOperation
        return next(); 
      } else  {
        const newlist : string[] = await todolist()
        newlist.push(ctx.message.text);
        await save_todolist(newlist);
        state = { kind : StateKind.NormalOp } as NormalOperation
        ctx.reply(`added TODO: ${ctx.message.text}`);
        rem(ctx)
      }
      break
    }
    case StateKind.EnterNewItemOp : {
      if ( ctx.message == undefined ) {
        state = { kind : StateKind.NormalOp } as NormalOperation
        return next(); 
      } else  {
        switch (currentList.kind) {
          case DefaultListKind.DLExists : {
            const lsts : LstMp = all_lists()
            const lst : string[] = lsts.get((currentList as DefaultList).id)
            lst.push(ctx.message.text)
            save_lists(lsts)
            show_list(ctx)
            state = { kind : StateKind.NormalOp } as NormalOperation
            break
          }
          case DefaultListKind.DLDoesNotExist : {
            state = { kind : StateKind.NormalOp } as NormalOperation
            lists(ctx)
            break
          }
        }
      }
      break
    }
  } 
})

bot.command('menu', menu)
bot.command('m', menu)
bot.command('help', menu)
bot.command('h', menu)
              
bot.command('lists', lists)
bot.command('list',show_list)

bot.action(/^CURLIST:(.+)/,
           async (ctx) => {
             await ctx.answerCbQuery()
             const lsts: LstMp = all_lists()
             const chs = ctx.match[1]
             for (const k of lsts.keys()) { 
               if (sha(k) === chs) { 
                 currentList = { kind : DefaultListKind.DLExists , id: k} as DefaultList 
                 //console.log(`current list: ${(currentList as DefaultList).id}`)
               }
             }
             show_list(ctx)
           }
          )

bot.hears(/^\/add$/,
          (ctx) => { switch (currentList.kind) {
            case (DefaultListKind.DLExists) : {
              ctx.reply("Enter new list item:")
              state = { kind : StateKind.EnterNewItemOp } as EnterNewItem
              break
            }
            case (DefaultListKind.DLDoesNotExist) : {
              ctx.reply("You have to first specify list!")
              break
            }
          }
          }
         )

bot.hears(/^\/add\s+(.+)/,
          (ctx) => { switch (currentList.kind) {
            case (DefaultListKind.DLExists) : {
              const lsts : LstMp = all_lists();
              const curId = (currentList as DefaultList).id;
              const lst : string[] = lsts.get(curId)
              lst.push(ctx.match[1])
              save_lists(lsts)
              show_list(ctx)
              break
            }
            case (DefaultListKind.DLDoesNotExist) : {
              ctx.reply("You have to first specify list!")
              break
            }
          }
          }
         )

bot.hears(/^\/newlist\s+(.+)/,
          (ctx) => {
            const lsts : LstMp = all_lists()
            const nm = ctx.match[1]
            if (lsts.has(nm)) {
              ctx.reply("list with such nm already exists")
            } else {
              //console.log(`adding new list: ${nm}`)
              lsts.set(nm,[])
              save_lists(lsts)
              lists(ctx)
            }
          }
         )

bot.command('delist',
            (ctx) => { switch (currentList.kind) {
              case DefaultListKind.DLExists : {
                const listId = (currentList as DefaultList).id
                state = { kind : StateKind.ListDeleteOp , checksum : sha(listId)  } as ListDeleteConfirmation ;
                ctx.reply(
                  `Should I delete the list: ${listId} ?`,
                  Extra.markdown().markup(Markup.keyboard([
                    ["Yes", "No", "/clear"],
                    ["/todo", "/rem"], 
                    ["/list", "/add", "/lists"], 
                    ["/menu"]
                  ]).oneTime().resize())
                )
                break
              }
              case DefaultListKind.DLDoesNotExist : {
                ctx.reply("ERROR: there is no currently selected list")
                break
              }
            }
            }
           )

bot.hears(/^\/rem$/,
          (ctx) => {
            state = { kind : StateKind.EnterNewRemOp } as EnterNewRem
            ctx.reply("Enter new TODO:")
          }
         )


bot.hears(/^\/rem\s+(.+)/, 
            async (ctx) => {
              const newlist : string[] = await todolist()
              newlist.push(ctx.match[1]);
              await save_todolist(newlist);
              ctx.reply(`added TODO: ${ctx.match[1]}`);
              rem(ctx)
            }
         )

bot.command('todo', rem)

bot.command('clear', 
            (ctx) => {
              ctx.reply(
                "Cleaning up...",
                Extra.markdown().markup(Markup.removeKeyboard())
              )
            }
           )

bot.action(/TODODEL:(.*)/,
           async (ctx) => {
             state = { kind : StateKind.TodoDeleteOp , checksum : ctx.match[1] } as TodoDeleteConfirmation ;
             const tdl = await todolist();
             const todel = tdl.filter((v) => sha(v) === ctx.match[1])
             await ctx.answerCbQuery();
             await ctx.reply(
               `Should I delete: ${JSON.stringify(todel)} ?`,
               Extra.markdown().markup(Markup.keyboard([["Yes", "No", "/menu"]]).oneTime().resize())
             )
           })

bot.action(/ITEMDEL:(.*)/,
           async (ctx) => {
             switch (currentList.kind) {
               case (DefaultListKind.DLDoesNotExist) : {
                 await ctx.answerCbQuery()
                 lists(ctx)
                 break
               }
               case (DefaultListKind.DLExists) : {
                 await ctx.answerCbQuery()
                 const lsts : LstMp = all_lists()
                 const lst : string[] = lsts.get((currentList as DefaultList).id) 
                 state = { kind : StateKind.ItemDeleteOp , checksum : ctx.match[1] } as ItemDeleteConfirmation ;
                 const todel = lst.filter((v) => sha(v) === ctx.match[1])
                 ctx.reply(
                   `Should I delete: ${JSON.stringify(todel)} ?`,
                   Extra.markdown().markup(Markup.keyboard([["Yes", "No", "/list", "/menu"]]).oneTime().resize())
                 )
                 break
               }
             }
           }
          )
           
console.log("TeleBot is launching...")

bot.launch()

